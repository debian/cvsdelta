Summary:   Summary and manager of CVS changes
Name:      cvsdelta
Version:   1.7.0
Release:   1
Epoch:     0
License:   LGPL
Group:     Development/Tools
URL:       http://cvsdelta.sourceforge.net/
Source:    http://prdownloads.sourceforge.net/cvsdelta/cvsdelta-%{version}.tar.gz
Requires:  ruby
BuildRoot: %{_tmppath}/%{name}-%{version}-root
BuildArch: noarch
Packager:  Jeff Pace (jpace@incava.org)
Vendor:    incava.org

%description
Summarizes the differences between a CVS project and local files, listing
the files and the number of lines that have been added, changed, and
removed. Can optionally execute the related CVS commands to add and
delete files.

%prep
%setup -q

%build

%install
[ "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf "$RPM_BUILD_ROOT"
mkdir -p "$RPM_BUILD_ROOT"%{_bindir}
mkdir -p "$RPM_BUILD_ROOT"%{_mandir}/man1/
cp cvsdelta "$RPM_BUILD_ROOT"%{_bindir}
cp cvsdelta.1 "$RPM_BUILD_ROOT"%{_mandir}/man1/

%clean
[ "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf "$RPM_BUILD_ROOT"

%files
%defattr(-,root,root,-)
%{_bindir}/cvsdelta
%{_mandir}/man1/cvsdelta.1*

%changelog
* Tue Apr 06 2004 Jeff Pace (jpace@incava.org) 1.7.0-1
- Fixed bug reporting not-yet-commited added and removed files
- Improved .spec file

* Wed Apr 23 2003 Andre Costa
- implemented correct use of %{buildroot} macro during 'make install'
- remove temp build dir on %clean
- use of predefined macros whenever possible
