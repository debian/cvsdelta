# Makefile for glark
# Borrowed/copied from ESR.

PACKAGE=cvsdelta
VERSION=1.7.0

prefix=/usr
mandir=$(prefix)/share/man
bindir=$(prefix)/bin

DOCS    = $(PACKAGE).1 $(PACKAGE).pod
SOURCES = $(PACKAGE) Makefile $(DOCS) $(PACKAGE).spec

all:

install: $(PACKAGE).1
	mkdir -p $(DESTDIR)$(bindir)
	mkdir -p $(DESTDIR)$(mandir)/man1
	cp -p $(PACKAGE) $(DESTDIR)$(bindir)
	# gzipping the man page conflicts with Mandrake, which bzips it:
	# gzip <$(PACKAGE).1 >$(DESTDIR)$(mandir)/man1/$(PACKAGE).1.gz
	cp -p $(PACKAGE).1 $(DESTDIR)$(mandir)/man1/$(PACKAGE).1

uninstall:
	rm -f $(DESTDIR)$(bindir)/$(PACKAGE)
	-rm -f $(DESTDIR)$(mandir)/man1/$(PACKAGE).1*
	-rm -f $(DESTDIR)$(prefix)/man/man1/glark.1*

# Builds the man page from the POD-format documentation.
$(PACKAGE).1: $(PACKAGE).pod
	pod2man --release "$(PACKAGE) $(VERSION)" \
		--center "$(PACKAGE) $(VERSION)" \
		$^ > $@

$(PACKAGE)-$(VERSION).tar.gz: $(SOURCES)
	@mkdir $(PACKAGE)-$(VERSION)
	@cp $(SOURCES) $(PACKAGE)-$(VERSION)
	@tar -czf $(PACKAGE)-$(VERSION).tar.gz $(PACKAGE)-$(VERSION)
	@rm -fr $(PACKAGE)-$(VERSION)

dist: $(PACKAGE)-$(VERSION).tar.gz

# The following rules are not for public use.

# Builds the HTML version of the man page.
doc/www/$(PACKAGE).html: $(PACKAGE).pod
	pod2html --noindex --verbose --css=$(PACKAGE)man.css $^ > $@
